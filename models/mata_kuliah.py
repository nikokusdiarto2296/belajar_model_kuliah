"""file mata kuliah"""
from odoo import models, fields, api, _

class MataKuliah(models.Model):
    """new model"""
    _name   = "matakuliah"
    _rec_name   = "nama"

    """field from mata kuliah"""
    kode_mata_kuliah    = fields.Char()
    nama                = fields.Char()
    jurusan_id          = fields.Many2one('jurusan')