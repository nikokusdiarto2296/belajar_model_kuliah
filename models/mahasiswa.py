"""file mahasiswa"""
from odoo import models, fields, api, _

class Mahasiswa(models.Model):
    """new model"""
    _name   = "mahasiswa"
    _rec_name = "nama"

    """fields from mahasiswa"""
    nik         = fields.Char()
    nama        = fields.Char()
    foto        = fields.Binary()
    jurusan_id  = fields.Many2one('jurusan', required=True)
    dosen_wali  = fields.Many2one('dosen')
    jadwal_ids  = fields.One2many('jadwalkuliah', 'mahasiswa_id')

    @api.model
    def create(self, vals):
        res = super(Mahasiswa, self).create(vals)
        res.nik = res.jurusan_id.sequence_id.next_by_id()
        return res