"""file jurusan"""
from odoo import models, fields, api, _

class Jurusan(models.Model):
    """new model"""
    _name = "jurusan"
    _rec_name = "nama_jurusan"

    """fields from jurusan"""
    kode_jurusan = fields.Char(required=True)
    nama_jurusan = fields.Char()
    sequence_id = fields.Many2one('ir.sequence')

    @api.model
    def create(self, vals):
        res = super(Jurusan, self).create(vals)
        res.sequence_id = self.env['ir.sequence'].create({
            'name': 'Jurusan '+res.nama_jurusan,
            'code': res.kode_jurusan,
            'prefix': res.kode_jurusan + '/%(year)s/',
            'padding': '6'
        })
        return res
    
    def write(self, vals):
        res = super(Jurusan, self).write(vals)
        if 'kode_jurusan' in vals:
            new_prefix = vals['kode_jurusan'] + '/%(year)s/'
            new_code = vals['kode_jurusan']
            self.sequence_id.write({
                'code' : new_code,
                'prefix': new_prefix
            })
        if 'nama_jurusan' in vals:  
            new_name = 'Jurusan ' + vals['nama_jurusan']
            self.sequence_id.write({
                'name': new_name
            })
        return res
