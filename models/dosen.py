"""file dosen"""
from odoo import models, fields, api, _

class Dosen(models.Model):
    """new model"""
    _name       = "dosen"
    _rec_name   = "nama"
    _inherit    = ['mail.thread', 'mail.activity.mixin'] 

    """field from dosen"""
    nomor_induk = fields.Char()
    nama        = fields.Char()