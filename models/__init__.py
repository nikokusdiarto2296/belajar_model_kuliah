# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from . import dosen
from . import jadwal_kuliah
from . import jurusan
from . import mahasiswa
from . import mata_kuliah
from . import ruang_kelas